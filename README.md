# Starter Kit Demo & Hands on

## 実行環境準備とOperatorのインストール
---
* ROSA環境を事前に構築  
* Operator Hubより"OpenShift Pipelines"と"OpenShift GitOps"をインストール  
  
  
## 利用するリポジトリ
---
本デモでは以下2種類のリポジトリを使用します。
これらのリポジトリを自身のGitLabアカウントを使いForkしておきましょう。

* starter-kit-cicd-manifest: 使用する各種マニフェストファイルを含むリポジトリ  
  https://gitlab.com/jpishikawa/starter-kit-cicd-manifest

* starter-kit-cicd-app: Patient Health Recordsのアプリケーションリポジトリ  
  https://gitlab.com/jpishikawa/starter-kit-cicd-app  



### **!! 以降の手順については`./demo` `./handson`の各ディレクトリの README.mdを参照 !!**
